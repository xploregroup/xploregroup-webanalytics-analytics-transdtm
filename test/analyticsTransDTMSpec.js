var assert = require('assert');
var sinon = require('sinon');

var MockBrowser = require('mock-browser').mocks.MockBrowser;
var mock = new MockBrowser();
global.window = mock.getWindow();
global.window.document = mock.getDocument();

var atDTM = require('../analyticsTransDTM');
var transInstance = atDTM();

describe('analyticsTransDTM', function () {
  it('should have analyticstracker', function () {
    assert.equal(typeof transInstance.tracker, 'object');
  });

  describe('sendPageImpressionEvent', function() {
    it('should pass page impression event', function(done) {
      var testEvent = {"event" : "page-impression", "info" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}};
      var resultEvent = {"event" : "page-impression", "pageInfo" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}}

      var sCE = sinon.spy(window, "CustomEvent");

      transInstance.tracker.trackEvent(testEvent);

      function testCustomEvent() {
        assert.equal(sCE.called, true);
        var theEventName = sCE.firstCall.args[0];
        var theEventDetail = sCE.firstCall.args[1].detail;
        assert.equal(theEventName, 'page-impression');
        assert.deepStrictEqual(theEventDetail, resultEvent);
        done();
      }
      setTimeout(testCustomEvent, 100);
    });
  });
});
