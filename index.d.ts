declare namespace analyticsTransDTM {
    function getInstance(): any;
    namespace tracker {
        const pageInfo: {
            category: string;
            domain: string;
            name: string;
            params: {
            };
            path: string;
            platform: string;
            referrer: string;
            timestamp: string;
            title: string;
            url: string;
            userAgent: string;
        };
        function clone(a: any): any;
        function getErrors(): any;
        function getEventFromElement(theElements: any): any;
        function getInstance(): any;
        function getVisibleImpressions(eventSelector: any): any;
        function register(eventList: any): void;
        function resetTracker(): void;
        function setHTMLSelectors(selectorSettings: any): void;
        function subscribe(subscribername: any, callback: any): any;
        function trackElement(theElements: any, options: any): any;
        function trackEvent(eventData: any): any;
        function trackImpression(eventSelector: any, options: any): any;
        function trackInteraction(event: any, options: any): any;
        function trackingSubscribe(subscribername: any, callback: any): any;
        namespace mediator {
            function bind(a: any, b: any, c: any, d: any): any;
            function emit(a: any, args: any[]): any;
            function getChannel(a: any, b: any): any;
            function getSubscriber(a: any, b: any): any;
            function off(a: any, b: any): any;
            function on(a: any, b: any, c: any, d: any): any;
            function once(a: any, b: any, c: any, d: any): any;
            function publish(a: any, args: any[]): any;
            function remove(a: any, b: any): any;
            function subscribe(a: any, b: any, c: any, d: any): any;
            function trigger(a: any, args: any[]): any;
        }
    }
}
